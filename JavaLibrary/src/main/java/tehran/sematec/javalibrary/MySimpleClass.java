package tehran.sematec.javalibrary;

public class MySimpleClass {

    private String city = "Tehran";
    private String country;

    private static String family;

    private void changeValue() {
        city = "karaj";
        country = "Iran";
        family = "Teymoori";
    }


    public static void main(String[] args) {
//        System.out.println("AmirHossein");
//        System.out.println(30);
//        System.out.println(true);
//        System.out.println(3.144444);

        printName("AmirHossein");

    }


    private static void printName(String name) {
        System.out.println(name);
    }


    private static String getMyName(int age) {
        if (age == 30)
            return "Amirhossein";
        return "";
    }

}
